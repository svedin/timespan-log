package eu.svedin.timespanlog;

import java.util.UUID;

import com.google.common.base.MoreObjects;

public class LogEntryId {

    private final String mId;

    public LogEntryId() {
        mId = UUID.randomUUID().toString();
    }

    public String getId() {
        return mId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", mId)
                .toString();
    }
}
