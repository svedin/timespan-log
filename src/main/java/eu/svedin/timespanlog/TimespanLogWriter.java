package eu.svedin.timespanlog;

import eu.svedin.timespanlog.logger.TimespanLogger;
import eu.svedin.timespanlog.logger.TimespanLogger.EntryType;

public class TimespanLogWriter {

    private static final String PACKAGE_NAME = TimespanLogWriter.class.getPackage().getName();

    private final TimespanLogger mLogger;

    public TimespanLogWriter(TimespanLogger logger) {
        mLogger = logger;
    }

    public void writeStartEntry(String logId, String message, boolean withStackTrace) {
        writeEntry(logId, EntryType.START, message, withStackTrace);
    }

    public void writeEndEntry(String logId, String message, boolean withStackTrace) {
        writeEntry(logId, EntryType.START, message, withStackTrace);
    }

    private void writeEntry(String logId, EntryType entryType, String message, boolean withStackTrace) {
        final long timestamp = System.currentTimeMillis();

        final String stackTrace;
        if (withStackTrace) {
            stackTrace = getStackTrace();
        } else {
            stackTrace = null;
        }

        mLogger.writeEntry(logId, entryType, timestamp, message, stackTrace);
    }

    private String getStackTrace() {
        final StackTraceElement[] stackTraceElements = new RuntimeException().getStackTrace();
        final StringBuilder stringBuilder = new StringBuilder();

        boolean foundStart = false;
        for (StackTraceElement element : stackTraceElements) {
            if (foundStart || !element.getClassName().startsWith(PACKAGE_NAME)) {
                foundStart = true;

                stringBuilder.append("\tat ").append(element).append("\n");
            }
        }

        return stringBuilder.toString();
    }
}
