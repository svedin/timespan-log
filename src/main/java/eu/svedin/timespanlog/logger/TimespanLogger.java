package eu.svedin.timespanlog.logger;

public interface TimespanLogger {

    public enum EntryType {
        START, STOP
    }

    void writeEntry(String logId, EntryType entryType, long timestamp, String message, String stackTrace);

}
