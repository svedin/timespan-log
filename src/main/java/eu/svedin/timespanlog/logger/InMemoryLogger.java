package eu.svedin.timespanlog.logger;

import java.util.Map;

import com.google.common.collect.Maps;

public class InMemoryLogger implements TimespanLogger {

    private final Map<String, LogData> mLog = Maps.newHashMap();

    public void writeEntry(String logId, EntryType type, long timestamp, String message, String stackTrace) {
        switch (type) {
        case START:
            mLog.put(logId, new LogData(timestamp, message, stackTrace));
            break;
        case STOP:
            mLog.get(logId).addEndData(timestamp, message, stackTrace);
            break;
        default:
            throw new RuntimeException("Unknown entry type");
        }
    }

    public Map<String, LogData> getLog() {
        return mLog;
    }

    public static class LogData {
        private final long mStartTime;
        private long mEndTime;
        private final String mStartMessage;
        private String mEndMessage;
        private final String mStartStackTrace;
        private String mEndStackTrace;

        public LogData(long startTime, String startMessage, String startStackTrace) {
            mStartTime = startTime;
            mStartMessage = startMessage;
            mStartStackTrace = startStackTrace;
        }

        public void addEndData(long endTime, String endMessage, String endStackTrace) {
            mEndTime = endTime;
            mEndMessage = endMessage;
            mEndStackTrace = endStackTrace;
        }
    }
}
