package eu.svedin.timespanlog;

import eu.svedin.timespanlog.logger.TimespanLogger;

public class TimespanLog {

    private final TimespanLogWriter mLogWriter;

    public TimespanLog(TimespanLogger logger) {
        mLogWriter = new TimespanLogWriter(logger);
    }

    public LogEntryId logStart(String message) {
        return logStart(message, true);
    }

    public LogEntryId logStart(String message, boolean withStackTrace) {
        final LogEntryId logId = new LogEntryId();
        mLogWriter.writeStartEntry(logId.getId(), message, withStackTrace);

        return logId;
    }

    public void logEnd(LogEntryId logId) {
        logEnd(logId, null, false);
    }

    public void logEnd(LogEntryId logId, String message, boolean withStackTrace) {
        mLogWriter.writeEndEntry(logId.getId(), message, withStackTrace);
    }
}
