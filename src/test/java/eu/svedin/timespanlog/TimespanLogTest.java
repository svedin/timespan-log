package eu.svedin.timespanlog;

import java.util.Map;

import eu.svedin.timespanlog.logger.InMemoryLogger;
import eu.svedin.timespanlog.logger.InMemoryLogger.LogData;
import junit.framework.TestCase;

public class TimespanLogTest extends TestCase {

    public void testLogging() {
        final InMemoryLogger logger = new InMemoryLogger();
        final TimespanLog log = new TimespanLog(logger);

        final LogEntryId logId = log.logStart("Starting");

        final Map<String, LogData> logEntries = logger.getLog();
        assertEquals(1, logEntries.size());
        assertTrue(logEntries.containsKey(logId.getId()));

        log.logEnd(logId);
        assertEquals(1, logEntries.size());
        assertTrue(logEntries.containsKey(logId.getId()));
    }
}
